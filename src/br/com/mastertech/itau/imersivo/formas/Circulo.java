package br.com.mastertech.itau.imersivo.formas;

import java.util.List;

public class Circulo extends Forma {

	public Circulo(String n, List<Double> l) {
		super(n, l);
	}

	@Override
	public double calculaArea() {

		return Math.PI * Math.pow(super.lados.get(0), 2);
	}

}
