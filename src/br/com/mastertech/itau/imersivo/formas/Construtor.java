package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Construtor {

	public static void constroiForma() {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Olá! bem vindo ao calculador de área 3 mil!");
		System.out.println("Basta informar a medida de cada lado que eu te digo a área :)");
		System.out.println("Vamos começar!");
		System.out.println("");
		System.out.println("Obs: digite -1 se quiser parar de cadastrar lados!");
		System.out.println("");

		Forma forma;
		List<Double> lados = new ArrayList<>();

		boolean deveAdicionarNovoLado = true;
		while (deveAdicionarNovoLado) {
			System.out.println("Informe o tamanho do lado " + (lados.size() + 1));

			double tamanhoLado = Double.parseDouble(scanner.nextLine());

			if (tamanhoLado <= 0) {
				deveAdicionarNovoLado = false;
			} else {
				lados.add(tamanhoLado);
			}
		}

		System.out.println("Lados cadastrados!");
		System.out.println("Agora vamos calcular a área...");

		if (lados.size() == 0) {
			System.out.println("Forma inválida!");
		} else if (lados.size() == 1) {
			System.out.println("Eu identifiquei um circulo!");
			forma = new Circulo("Circulo", lados);
			calculaTamanho(forma);
			
		} else if (lados.size() == 2) {
			System.out.println("Eu identifiquei um quadrado/retangulo!");
			forma = new Quadrado("Quadrado", lados);
			calculaTamanho(forma);

		} else if (lados.size() == 3) {
			System.out.println("Eu identifiquei um triangulo!");
			forma = new Triangulo("Triangulo", lados);
			calculaTamanho(forma);

		} 
		
		else if (lados.size() == 4) {
			System.out.println("Eu identifiquei um losango!");
			forma = new Losango("Losango", lados);
			calculaTamanho(forma);
		} 		
		
		else {
			System.out.println("Ops! Eu não conheço essa forma geometrica ¯\\_(⊙_ʖ⊙)_/¯");
		}

	}

	private static void calculaTamanho(Forma f) {
		System.out.println("O tamanho do " + f.nome + " é: " + f.calculaArea());
	}

}
