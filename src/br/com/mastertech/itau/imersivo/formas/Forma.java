package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;

public abstract class Forma {

	protected String nome;
	protected List<Double> lados;

	public Forma(String n, List<Double> l) {
		this.nome = n;
		this.lados = l;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Double> getLados() {
		return lados;
	}

	public void setLados(List<Double> lados) {
		this.lados = lados;
	}

	public abstract double calculaArea();

}
