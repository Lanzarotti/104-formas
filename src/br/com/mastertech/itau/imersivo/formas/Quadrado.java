package br.com.mastertech.itau.imersivo.formas;

import java.util.List;

public class Quadrado extends Forma {

	public Quadrado(String n, List<Double> l) {
		super(n, l);
		
	}

	@Override
	public double calculaArea() {
		return lados.get(0) * lados.get(0);
	}

}
