package br.com.mastertech.itau.imersivo.formas;

import java.util.List;

public class Triangulo extends Forma {

	public Triangulo(String n, List<Double> l) {
		super(n, l);
	}

	private double ladoA = lados.get(0);
	private double ladoB = lados.get(1);
	private double ladoC = lados.get(2);

	@Override
	public double calculaArea() {
		if ((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA) {
			double s = (ladoA + ladoB + ladoC) / 2;
			double area = Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
			System.out.println("A área do triangulo é " + (area));
			return area;
		} else {
			System.out.println("Mas o triangulo informado era inválido :/");
			return 0;
		}

	}

}
